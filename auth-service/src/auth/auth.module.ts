import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

const Mongoose = MongooseModule.forFeature([]);

@Module({
  imports: [ConfigModule.forRoot(), Mongoose],
  controllers: [],
  providers: [],
  exports: [Mongoose],
})
export class AuthModule {}
