# Techify APACHE Gateway

Prueba tecnica para Backend Developer con NestJS

#### Check list

AUTH: Pipes, UseGuards, Custom Decorators, Metadata

TRANSPORT: DTOs, RPC, MessagePattern

ARQ: Controller-Service-Repository

DATA: Models, Schemas

EXTRA: Interface VS Object, POO
